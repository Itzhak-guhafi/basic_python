import re


def is_right_size(password: str) -> bool:
    minimum = 8
    maximum = 20

    return minimum < len(password) < maximum


def is_contains_two_capital_letters(password: str) -> bool:
    capital_letters = list(current_letter for current_letter in password if current_letter.isupper())
    return len(capital_letters) > 1


def is_contains_two_numbers(password: str) -> bool:
    numbers = list(current_letter for current_letter in password if current_letter.isnumeric())
    return len(numbers) > 1


def _create_regex(letters: str, password: str):
    return re.search(re.compile(f'[{letters}]'), password)


def is_contains_special_characters(password: str) -> bool:
    return True if _create_regex(letters='!@#$%^&*', password=password) else False


def is_there_letter_wrong(password: str) -> bool:
    return False if _create_regex(letters=';', password=password) else True


if __name__ == '__main__':
    user_password = input("Enter your password: ")
    print(is_right_size(password=user_password) and
          is_contains_two_capital_letters(password=user_password) and
          is_contains_two_numbers(password=user_password) and
          is_contains_special_characters(password=user_password) and
          is_there_letter_wrong(password=user_password))
