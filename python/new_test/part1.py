import string
import re


def get_amount_of_letters_of_the_file(file_name: str) -> None:
    amount_of_letters = 0
    for current_letter in file_name:

        # Checks if the current letter is a word
        if current_letter.lower() in string.ascii_lowercase:
            amount_of_letters += 1
    print(f'The total letters in the file: {amount_of_letters}')


def how_many_different_words(file_name: str) -> None:
    different_words = []
    for current_letter in file_name:
        if current_letter in string.ascii_lowercase and current_letter not in different_words:
            different_words.append(current_letter)

    print(f"Amount of different words: {len(different_words)}")


def get_amount_of_sentences(file_name: str) -> None:
    print(re.split('. |!|\?', file_name))


def get_the_longest_word(file_name: str) -> None:
    words = file_name.split()
    max_len = len(max(words, key=len))
    longest_word = [word for word in words if len(word) == max_len]
    print(f"The longest word is: {longest_word[0]}")


if __name__ == '__main__':
    my_file_name = input("Enter your file name: ")
    try:
        my_file = open(my_file_name, 'r')

        get_amount_of_letters_of_the_file(file_name=my_file.read())
        my_file.seek(0)

        how_many_different_words(file_name=my_file.read())
        my_file.seek(0)

        get_amount_of_sentences(file_name=my_file.read())
        my_file.seek(0)

        get_the_longest_word(file_name=my_file.read())

        my_file.close()
    except:
        print("Can't find file")
