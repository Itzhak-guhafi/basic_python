from typing import List


def find_the_second_largest_number_in_list(my_list: List[int]):
    my_list.sort()
    print(my_list[-2])


if __name__ == '__main__':
    test_list = [10, 70, 20, 4, 45, 99]
    find_the_second_largest_number_in_list(test_list)
