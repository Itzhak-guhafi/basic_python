import string


def is_contains_all_letters_of_the_alphabet(my_str: str):
    alphabet = set(string.ascii_lowercase)
    print(set(my_str.lower()) >= alphabet)


if __name__ == '__main__':
    good_string = 'The quick brown fox jumps over the lazy dog'
    bad_string = 'The quick brown fox jumps over the lazy cat'
    is_contains_all_letters_of_the_alphabet(bad_string)
