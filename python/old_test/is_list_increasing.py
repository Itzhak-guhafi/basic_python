from typing import List


def check_if_list_is_increasing(my_list: List[int]):
    res = all(i < j for i, j in zip(my_list, my_list[1:]))
    print(res)


if __name__ == '__main__':
    test_list = [1, 4, 5, 7, 8, 10]
    check_if_list_is_increasing(test_list)
