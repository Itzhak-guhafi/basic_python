from typing import Any


def remove_element_from_list_by_index(index: int, my_list: list[Any]):
    my_list.pop(index)
    print(my_list)


if __name__ == '__main__':
    a = ['a', 'b', 'c', 'd']
    remove_element_from_list_by_index(0, a)
